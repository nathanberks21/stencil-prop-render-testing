import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'child-new-array-component',
  shadow: false
})
export class ChildNewArrayComponent {
  @Prop() values: number[];

  renderCallCount = 0;

  render() {
    return [
      <h1>Not watching prop render calls: {++this.renderCallCount}</h1>,
      this.values.map(v => <p>{v}</p>)
    ];
  }
}
