import { Component, Prop, Watch } from '@stencil/core';

@Component({
  tag: 'child-watch-component',
  shadow: false
})
export class ChildWatchComponent {
  @Prop() values: number[];

  @Watch('values')
  onValuesChange() {}

  renderCallCount = 0;

  render() {
    return [
      <h1>Watching prop render calls: {++this.renderCallCount}</h1>,
      this.values.map(v => <p>{v}</p>)
    ];
  }
}
