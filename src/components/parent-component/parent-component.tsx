import { Component, State } from '@stencil/core';

@Component({
  tag: 'parent-component',
  shadow: false
})
export class ParentComponent {
  @State() arr = [1, 2, 3];

  constructor() {
    this.assignNewArr = this.assignNewArr.bind(this);
    this.assignNewSpreadCopyArr = this.assignNewSpreadCopyArr.bind(this);
  }

  render() {
    return [
      <child-new-array-component values={this.arr} />,
      <child-watch-component values={this.arr} />,
      <button onClick={this.assignNewArr}>Assign New Array</button>,
      <button onClick={this.assignNewSpreadCopyArr}>
        Assign New Spread Copy Array
      </button>
    ];
  }

  assignNewArr() {
    const newArr = [
      this.randomNumber(),
      this.randomNumber(),
      this.randomNumber()
    ];
    this.arr = newArr;
  }

  assignNewSpreadCopyArr() {
    const newArr = [
      this.randomNumber(),
      this.randomNumber(),
      this.randomNumber()
    ];
    this.arr = [...newArr];
  }

  randomNumber() {
    return Math.floor(Math.random() * 1000);
  }
}
