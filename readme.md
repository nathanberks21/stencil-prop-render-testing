## StencilJS Render Prop Testing

Playing with the interaction with how Stencil triggers the render function for child components when the prop changes.

It's important to remember that `@State` should be used for values that may change and need to be forwarded to child components via props. Otherwise the render function is not triggered when those values update.

An alternative to this would be to assign the value directly to the selected element, if selecting via JavaScript. Though this method isn't making the most use out of Stencil's lifecycle functionality.
